CREATE TABLE movies (
	id serial PRIMARY KEY,
	title VARCHAR ( 50 ) UNIQUE NOT NULL,
	room VARCHAR ( 50 ) NOT NULL,
	time TIMESTAMP NOT NULL,
	empty_seats VARCHAR (200)
);

CREATE TABLE bookings (
	id serial PRIMARY KEY,
	tenant VARCHAR ( 50 ) UNIQUE NOT NULL,
	reserved_seats VARCHAR ( 50 ) NOT NULL,
	movie_id INTEGER REFERENCES movies
);

INSERT INTO movies(id, title, room, time, empty_seats)
VALUES (0, 'The great Gatsby', '10', '2003-04-12 04:05:06', 'A1-B1' );
INSERT INTO movies(id, title, room, time, empty_seats)
VALUES (1, 'Fast and Furious 1', '9', '2003-04-12 04:05:06', 'A1-B1' );
INSERT INTO movies(id, title, room, time, empty_seats)
VALUES (2, 'Fast and Furious 2', '10', '2003-04-12 06:05:06', 'A1-B1' );
INSERT INTO movies(id, title, room, time, empty_seats)
VALUES (3, 'Fast and Furious 3', '7', '2003-04-12 02:00:06', 'A1-B1' );
INSERT INTO movies(id, title, room, time, empty_seats)
VALUES (4, 'Fast and Furious 9', '10', '2003-04-18 04:05:06', 'A1-B1' );
INSERT INTO movies(id, title, room, time, empty_seats)
VALUES (5, 'American Pie 1', '1', '2003-04-12 12:05:06', 'A1-B1' );
INSERT INTO movies(id, title, room, time, empty_seats)
VALUES (6, 'American Pie 2', '2', '2003-04-13 04:05:06', 'A1-B1' );
INSERT INTO movies(id, title, room, time, empty_seats)
VALUES (7, 'Batman', '10', '2003-04-12 08:05:06', 'A1-B1' );
INSERT INTO movies(id, title, room, time, empty_seats)
VALUES (8, '3 Days to Kill', '10', '2003-04-22 05:05:06', 'A1-B1' );