# MOVIE BOOKING SERVICE

### Containerized version of the app.

(C) Copyright 2021

The team:

- Călătoaie Iulia-Adriana 343 C1

- Georgescu Alin-Andrei 342 C3

- Millio Anca 342 C4

- Negru Bogdan-Cristian 342 C3

This is a cinema movie booking web application.

The app functionalities are:

- GET "/api/v1/movies/info?perPage=10&page=0"
  - retrieve information about all available movies
  - return 200 OK and a list of movies

- POST "/api/v1/booking/{user}/booking/{movieId}"
  - book one or more seats
  - return 200 OK and the booking id or 404 Not Found with 2 possible messages
  (Movie not found. / Seat unavailable.)

- GET "/api/v1/booking/{user}/bookings/{bookingId}
  - get a booking information
  - return 200 OK and a booking info or 404 Not Found with a message
  (Booking not found.)

- GET "/api/v1/booking/{user}/bookings"
  - get a user's bookings
  - return 200 OK and a list of bookings' information

- DELETE "/api/v1/booking/{user}/bookings/{bookingId}
  - delete a user's bookings
  - return 200 OK

The routes can be accessed only by using Basic Auth and having a valid username
and password.

**Kong Gateway** runs on port 80.

**Adminer** runs on port 8080.

**Portainer** runs on port 9000.

---

How to run from CLI:

1. Open the current folder in a terminal / Powershell instance.

2. Init a Docker swarm.

3. docker stack deploy -c stack.yml movie

4. Access the app on port 80 and with the URI "/auth".

5. docker stack rm movie

---

How to run from Portainer:

1. Open the current folder in a terminal / Powershell instance.

2. Init a Docker swarm.

3. docker stack deploy -c stack_portainer.yml portainer

4. Access the Portainer on port 9000.

5. Launch and configurate the app's swarm from Portainer's GUI.

6. docker stack rm portainer
